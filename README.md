# Scout Plane
Scout Plane makes it trivial to get sosreports and other files ready for analysis in a consistent 
and containerized manner. It is designed to work in concert with a RemoteSFTP system that houses 
said files.

Please open Issues on how this could be changed to better fit your workflow. 
Also, if you'd like to discuss or work on one of the TODOs please open an issue 
for it. 

If you are working on it, please make the pull requests do one thing at a time 
so they are easier to merge and less likely to break other work.  At the moment 
the code is rough - but evolving as we get a better idea of what's needed.

## Requirements
 * Python3
 * lxd 3.0 installed and working (currently does not work with LXD 2.X)
 * python3-paramiko python3-pylxd
 * Currently only known to be working on 18.04
 * LXD clustering may work at the moment, needs testing
 
## Current Status matrix 
| sp Install | LXD Install | 16.04                                                       | 18.04  | 
|:----------:|:-----------:|:-----------------------------------------------------------:|:------:|
| git        | Snapped LXD | Needs fresh testing - switched the part that was breaking it | Works! |  
| git        | Deb LXD     | -updates too old, haven't tried backports 2.21               | Works! | 

## Setup
1. Run sp the first time, it will configure your username and remote server
2. Run sp the second time, it will create the needed image for you
3. Make sure you've connected to the server with SSH before (so you have it's host key)
4. Make sure the host has your SSH public key (for some services - BrickFTP you have to add it in the web interface)

## General Usage
0. Have something that unlocks your SSH key, ssh-add, gnomey things
1. Get notified by that a new file is on RemoteSSH server at $filepath
2. Run ./sp $filepath
3. Login to container and find file (and if sosreport extracted and automatically analyzed via xsos - other tools TODO)

# ScoutPlane (sp.py)
    ./sp [target-sosreport-path-on-remote-system]
  1. Launch new LXD container
  2. Fetch target sosreport to container from RemoteSSH system
  3. Launch sosreport analysis routine in container
  4. Log build & process output (IP)

## TODO
  * Generally better error checking
  * batch process on a folder's contents (aka targetting a whole case instead of one file)
  * check before downloading a file again, inform user
  * Evalaute switching from xsos to pysos
  * Push analysis (xsos) reports back to RemoteSTP system
  * APPARMOR tar/xsos, other processes that process untrusted data for another level of protection (evaluate switching to Ubuntu core?)
  * CI