#!/usr/bin/env python3
# Helpful LXD creation bits

from time import sleep
import pylxd
client = pylxd.Client()


def StartContainer(container):
    print("..Starting Container", end='', flush=True)
    if container.status != "Running":
        container.start(wait=True)
    else:
        print("**Already Running**")
    # Blah, systemd goes into degraded state in 18.04 so we can't use exit codes
    # If you switch to 19.04 you can use exit code of 0
    while container.execute(['systemctl', 'is-system-running']).stdout != "degraded\n":
        sleep(0.1)
        print(".", end='', flush=True)
    print("\n..Started Container")
    return


def createlxdimage():
    container_name = "sp-setup"
    image_name = "sp-base-image"
    config = {'name': container_name, 'source': {'type': 'image', "mode": "pull", "server":
              "https://cloud-images.ubuntu.com/daily/", "protocol": "simplestreams", 'alias': 'bionic/amd64'}}

    if client.containers.exists(container_name):
        print("..container name already exists, deleting")
        container = client.containers.get(container_name)
        if container.status != "Stopped":
            container.stop(wait=True)
        container.delete(wait=True)

    try:
        lxd_image = client.images.get_by_alias("sp-base-image")
        lxd_image.delete(wait=True)
        print("..deleting existing image")
    except pylxd.exceptions.NotFound as e:
        print("..No Image previously setup")

    container = client.containers.create(config, wait=True)
    StartContainer(container)

    print("..Start making changes")
    print("...." + str(container.execute(['snap', 'install', 'xsos']).exit_code))
    print("...." + str(container.execute(['apt-get', 'purge', 'rsyslog', 'lxd', 'lxd-client', '--yes', '--quiet']).exit_code))
    print("...." + str(container.execute(['apt-get', 'install', 'tree', 'lnav', '--yes', '--quiet']).exit_code))
    print("..Done making changes")

    print("..Stopping Container")
    container.stop(wait=True)

    print("..Publishing Image")
    image = container.publish(wait=True)
    image.add_alias(name="sp-base-image", description="ScoutPlane image")

    print("..Deleting Container")
    container.delete(wait=True)
    return


if __name__ == "__main__":
    createlxdimage()

# vim: set et ts=4 sw=4 :
